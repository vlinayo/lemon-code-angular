import { Component, OnInit, Output } from '@angular/core';
import { Movie } from '../movies.model';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.css']
})
export class MovieListComponent implements OnInit {
  showMovie: Movie;

  @Output() movies= [
    new Movie('The Shawshank Redemption (1994)','Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.',
    'https://m.media-amazon.com/images/M/MV5BMDFkYTc0MGEtZmNhMC00ZDIzLWFmNTEtODM1ZmRlYWMwMWFmXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_.jpg'),
    new Movie('The Godfather (1972)','The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.',
    'https://m.media-amazon.com/images/M/MV5BM2MyNjYxNmUtYTAwNi00MTYxLWJmNWYtYzZlODY3ZTk3OTFlXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,704,1000_AL_.jpg'),
    new Movie('The Godfather: Part II (1974)','The early life and career of Vito Corleone in 1920s New York City is portrayed, while his son, Michael, expands and tightens his grip on the family crime syndicate.',
    'https://m.media-amazon.com/images/M/MV5BMWMwMGQzZTItY2JlNC00OWZiLWIyMDctNDk2ZDQ2YjRjMWQ0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_SY1000_CR0,0,701,1000_AL_.jpg'),
  ];

  constructor() { }

  ngOnInit() {
  }

  movieSelected(movie: Movie){
    // console.log(movie);
    this.showMovie = movie;

  }

}
