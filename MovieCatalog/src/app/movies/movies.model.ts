export class Movie {
    public title: string;
    public synopsis: string;
    public imagePath: string;

    constructor(name: string, synopsis: string, img:string){
        this.title=name;
        this.synopsis=synopsis;
        this.imagePath=img;
    }
}