import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ShellComponent } from './home/shell.component';
import { WelcomeComponent } from './home/welcome.component';
import { PageNotFoundComponent } from './home/page-not-found.component';

@NgModule({
  imports: [CommonModule, RouterModule.forRoot([
    {
      path: '',
      component: ShellComponent,
      children: [
        { path: 'welcome', component: WelcomeComponent },
        { path: 'games', loadChildren: './games/games.module#GamesModule' },
        { path: '', redirectTo: 'welcome', pathMatch: 'full' },
      ]
    },
    { path: '**', component: PageNotFoundComponent } 
  ])],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
