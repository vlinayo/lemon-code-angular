import { Component, OnInit, ViewChild, ElementRef, ViewChildren, QueryList, AfterViewInit, Output } from '@angular/core';
// import { NgModel } from '@angular/forms';

// import { tap } from 'rxjs/operators';

import { IGame } from '../game.model';
import { GameService } from '../game.service';
import { CriteriaComponent } from 'src/app/shared/criteria/criteria.component';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css']
})
export class GameListComponent implements OnInit, AfterViewInit   {
  // listFilter: string;
  // @ViewChild('filterElement') filterElementRef: ElementRef; //hace referencia a un elemento nativo
  // @ViewChild(NgModel) filterInput: NgModel; // el tipado aqui es el de NgModel

  // una forma de usar ViewChildren():  
  // @ViewChildren('filterElement, nameElement') inputElementsRef: QueryList<ElementRef>;
  // una alternativa:
  // @ViewChildren(NgModel) inputElementsRef: QueryList<ElementRef>;

  // private _listFilter: string;  
  // get listFilter() : string {
  //   return this._listFilter;
  // }
  
  // set listFilter(v : string) {
  //   this._listFilter = v;
  //   this.performFilter(this.listFilter);
  // }
  
  @ViewChild(CriteriaComponent)filterComponent: CriteriaComponent;
  parentListFilter: string;
  
  filteredGames: IGame[];
  games: IGame[];
  showImage: boolean;
  includeDetail:boolean = true;

  imageWidth = 50;
  imageMargin = 2;

  constructor(private gameService: GameService) {
    // console.log(this.filterElementRef);
   }

  ngOnInit() {
    this.gameService.getGames().subscribe(
      (games: IGame[]) => {
        this.games = games;
        // this.performFilter();
        this.performFilter(this.parentListFilter);
      }
    );
  }

  ngAfterViewInit(): void {
    this.parentListFilter = this.filterComponent.listFilter;
  }

  onValueChange(value: string): void {
    this.performFilter(value);
  }

  toggleImage(): void {
    this.showImage = !this.showImage;
  }

  performFilter(filterBy?: string): void {
    if (filterBy) {
      this.filteredGames = this.games
        .filter(
          (g: IGame) =>
          g.name.toLocaleLowerCase()
            .indexOf(filterBy.toLocaleLowerCase()) !== -1
        );
    } else {
      this.filteredGames = this.games;
    }
  }

}