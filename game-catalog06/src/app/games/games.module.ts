import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/shared.module';

import { GameListComponent } from './game-list/game-list.component';

import { GameService } from './game.service';

@NgModule({
  declarations: [GameListComponent],
  providers: [
    GameService
  ],
  imports: [
    // CommonModule
    SharedModule,
    RouterModule.forChild([ 
      { path: '', component: GameListComponent }
    ]),
  ]
})
export class GamesModule { }
