import { Injectable } from '@angular/core';

import { IUser } from './user.model'

@Injectable()
export class AuthService {
  currentUser: IUser | null;
  redirectUrl: string;

  constructor() { }

  isLoggedIn(): boolean {
    return !!this.currentUser; // !! casting to boolean.
  }

  login(userName: string, password: string): void {
    // TODO: Implement backend service
    this.currentUser = {
      id: 34,
      isAdmin: false,
      userName // after EC6 we can do this.. instead of userName: userName
    };
  }

  logout(): void {
    this.currentUser = null;
  }
}
