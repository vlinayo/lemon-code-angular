import { NgModule } from '@angular/core';
// import { CommonModule } from '@angular/common';
import { AuthService } from './auth.service';
import { AuthGuardService } from './auth-guard.service';

import { SharedModule } from '../shared/shared.module';

import { LoginComponent } from './login.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [LoginComponent],
  imports: [
    // CommonModule,
    SharedModule,
    RouterModule.forChild([
      {path: 'login' , component: LoginComponent }
    ])
  ],
  providers: [AuthService, AuthGuardService]
})
export class UserModule { }
