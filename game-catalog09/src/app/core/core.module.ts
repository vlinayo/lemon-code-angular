import {
  NgModule
} from '@angular/core';
import { CommonModule } from '@angular/common';

import { HTTP_ERROR_HANDLER, handleError } from './http-data-error-handler.service';
import { HTTP_DATA_LOGGER, logJSON } from './http-data-logger.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    {
      provide: HTTP_DATA_LOGGER,
      useValue: {
        logJSON
      }
    },
    {
      provide: HTTP_ERROR_HANDLER,
      useValue: {
        handleError
      }
    }
  ]
})
export class CoreModule { }
