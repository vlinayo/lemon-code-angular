import { Component, OnInit, ViewChild, AfterViewInit, ElementRef, QueryList, ViewChildren } from '@angular/core';
import { NgModel } from '@angular/forms';

import { IGame } from '../game.model';
import { GameService } from '../game.service';
import { CriteriaComponent } from 'src/app/shared/criteria/criteria.component';
import { GameParameterService } from '../game-parameter.service';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.css']
})
// export class GameListComponent implements OnInit, AfterViewInit {
export class GameListComponent implements OnInit, AfterViewInit {
  // showImage: boolean;
  includeDetail: boolean = true;
  imageWidth = 50;
  imageMargin = 2;

  get showImage(): boolean{
    return this.gameParamsService.showImage;

  }

  set showImage(value: boolean){
    this.gameParamsService.showImage = value;
  }

  @ViewChild(CriteriaComponent)filterComponent: CriteriaComponent;
  parentListFilter: string;

  filteredGames: IGame[];
  games: IGame[];

  constructor(private gameService: GameService,
    private gameParamsService: GameParameterService) {}
  /*diff*/
  ngAfterViewInit(): void {
    this.parentListFilter = this.filterComponent.listFilter;
  }
  /*diff*/
  ngOnInit() {
    this.gameService.getGames().subscribe(
      (games: IGame[]) => {
        this.games = games;
        this.filterComponent.listFilter = this.gameParamsService.filterBy;
        // this.performFilter(this.parentListFilter);
      }
    );
  }

  onValueChange(value: string): void {
    this.performFilter(value);
    this.gameParamsService.filterBy = value;
  }

  toggleImage(): void {
    this.showImage = !this.showImage;
  }

  performFilter(filterBy?: string): void {
    if (filterBy) {
      this.filteredGames = this.games
        .filter(
          (g: IGame) =>
          g.name.toLocaleLowerCase()
            .indexOf(filterBy.toLocaleLowerCase()) !== -1
        );
    } else {
      this.filteredGames = this.games;
    }
  }

}
