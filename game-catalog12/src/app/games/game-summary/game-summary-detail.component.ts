import { Component, OnInit, OnDestroy } from '@angular/core';
import { IGame } from '../game.model';
import { GameService } from '../game.service';
// import { timer } from 'rxjs';
import {  Subscription } from 'rxjs';

@Component({
  selector: 'app-game-summary-detail',
  templateUrl: './game-summary-detail.component.html',
  styles: []
})
export class GameSummaryDetailComponent implements OnInit, OnDestroy {
  // game = this.gameService.currentGame;

  // get game(): IGame {
  // // get g(): IGame {
  //   return this.gameService.currentGame;
  // }
  game: IGame | null;
  gamSub : Subscription;

  constructor(private gameService: GameService) { }

  ngOnInit() {
    // timer(0, 1000).subscribe(
    //   () => console.log(this.g)
    // );

    this.gamSub = this.gameService.selectedGameChange$.subscribe(
      (g) => this.game = g
    );
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.gamSub.unsubscribe();
  }
  

}
