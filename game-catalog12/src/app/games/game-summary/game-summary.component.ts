import { Component, OnInit } from '@angular/core';
import { GameService } from '../game.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-game-summary',
  templateUrl: './game-summary.component.html',
  styles: []
})
export class GameSummaryComponent implements OnInit {
  yearCount: number;
  constructor(private gameService : GameService) { }

  dataRangeToYears = (start:Date, end: Date= new Date()) => (
    end.getFullYear() - start.getFullYear()
  );

  ngOnInit() {
    this.gameService.selectedGameChange$
      .pipe(
        map(
          (selectedGame) => {
            // const { release } = selectedGame; peta si selectedGame null..
            if( selectedGame ){
              const { release } = selectedGame;
              return this.dataRangeToYears(new Date(release));
            }
            return 0;
          })
      ).subscribe(
        (m) => this.yearCount = m);
  }



}
