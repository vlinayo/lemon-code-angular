import { Action } from  '@ngrx/store';
import { IGame } from '../game.model';


export enum GamesActionsTypes {
    SetCurrentFilter = '[ Games ] Set Filter',
  
    SetSelectedGame = '[ Games ] Set selected game',
    SaveSelectedGame = '[ Games ] Save selected game',
    DeleteSelectedGame = '[ Games ] Delete selected game',
    UpdateSelectedGame = '[ Games ] Update selected game',
    AddNewGame = '[ Games ] Add new game',
}

export class SetCurrentFilter implements Action {
    readonly type = GamesActionsTypes.SetCurrentFilter;
    constructor(public payload: string) {}
}

export class SetSelectedGame implements Action {
    readonly type = GamesActionsTypes.SetSelectedGame;
    constructor(public payload: IGame) {}
}

export class SaveSelectedGame implements Action {
    readonly type = GamesActionsTypes.SaveSelectedGame;
    constructor(public payload: IGame) {}
}

export class DeleteSelectedGame implements Action {
    readonly type = GamesActionsTypes.DeleteSelectedGame;
    constructor(public payload: IGame) {}
}

export class UpdateSelectedGame implements Action {
    readonly type = GamesActionsTypes.UpdateSelectedGame;
    constructor(public payload: IGame) {}
}

export class AddNewGame implements Action {
    readonly type = GamesActionsTypes.AddNewGame;
    constructor(public payload: IGame) {}
}


export type GamesActions = SetCurrentFilter |
    SetSelectedGame | SaveSelectedGame | DeleteSelectedGame |
    UpdateSelectedGame | AddNewGame;