import * as fromRoot from '../../state/app.state';
import { IGame } from '../game.model';
import { createFeatureSelector } from '@ngrx/store';
import { GamesActions } from './game.actions';


export interface GameState {
    currentFilter: string;
    selectedGame: IGame;
    games: IGame[];
}

export interface State extends fromRoot.State{
    games: GameState;
}

const initialState: GameState = {
    currentFilter: '',
    selectedGame: null,
    games: []
}

const getGamesFeatureState = createFeatureSelector<GameState>('games');

export const reducer =  ( state: GameState = initialState, 
    action: GamesActions ): GameState => {
        switch (action.type) {
            default:
                return state;
                // break;
        }
    }