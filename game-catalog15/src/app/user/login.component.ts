import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from './auth.service';
import { Store, select } from '@ngrx/store';

import * as fromUser from './state/user.reducer';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  errorMessage: string;
  pageTitle = 'Log In';

  maskUserName: boolean;

  constructor(
    private store: Store<fromUser.State>,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
    
    // this.store.pipe(
    //   select('userName') 
    // ).subscribe( 
    //   (usrName) => {
    //     if (usrName) { 
    //       this.maskUserName = usrName.showUserName; 
    //     }
    //   }
    // );
    this.store.pipe(select(fromUser.getShowUser)).subscribe(
      (s) => this.maskUserName = s
    );

  }

  cancel(): void {
    this.router.navigate(['welcome']);
  }

  checkChanged(value: boolean): void {
    // this.maskUserName = value;
    this.store.dispatch({
      type: 'MASK_USERNAME_DATA',
      payload: value,
    })
  }

  login(loginForm: NgForm): void {
    if (loginForm && loginForm.valid) {
      const { userName, password} = loginForm.form.value;
      // TODO: Implement as async / await
      this.authService.login(userName, password);

      if (this.authService.redirectUrl) {
        this.router.navigateByUrl(this.authService.redirectUrl);
      } else {
        this.router.navigate(['/games']);
      }
    }
  }

}
