import { IUser } from '../user';
import * as fromRoot from '../../state/app.state';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export interface userState {
    currentUser : IUser,
    showUserName: boolean
}

export interface State extends fromRoot.State{
   userState: userState;
}

const initialState: userState = {
    currentUser : null,
    showUserName: false
}

const getUserFeatureState = createFeatureSelector<userState>('userName');

export const getShowUser = createSelector(
    getUserFeatureState,
    state => state.showUserName
);

export const getCurrentUser = createSelector(
    getUserFeatureState,
    state => state.currentUser
);

export const reducer = (state: userState = initialState, action): userState => {
    switch (action.type) {
        case 'MASK_USERNAME_DATA':
            console.log( `existing state ${JSON.stringify(state)}`);
            console.log( `action payload ${action.payload}`);
            return {
                ...state,
                showUserName: action.payload,
            };
            // break;
        default:
            return state;
            // break;
    }
};