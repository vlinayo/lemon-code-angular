import { Action } from  '@ngrx/store';
import { VideoConsoleModel } from '../video-console.model';


export enum VideoConsolesActionTypes {
    ToggleVideoConsoleCode = '[Video Console] Toggle Video Console Code',
    SetCurrentVideoConsole = '[Video Console] Set current Video console',
    ClearCurrentVideoConsole = '[Video Console] Clear current Video console',
    InitializeCurrentVideoConsole = '[Video Console] Initialize current Video console',
    Load = '[Video Console] Load',
    LoadSuccess = '[Video Console] Success',
    LoadFailed = '[Video Console] Failed',

}

export class ToggleVideoConsoleCode implements Action {
    readonly type = VideoConsolesActionTypes.ToggleVideoConsoleCode;

    constructor(public payload: boolean) {
    }
}

export class SetCurrentVideoConsole implements Action {
    readonly type = VideoConsolesActionTypes.SetCurrentVideoConsole;

    constructor(public payload: VideoConsoleModel) {
    }

}

export class ClearCurrentVideoConsole implements Action {
    readonly type = VideoConsolesActionTypes.ClearCurrentVideoConsole;
    
}

export class InitializeCurrentVideoConsole implements Action {
    readonly type = VideoConsolesActionTypes.InitializeCurrentVideoConsole;
    
}

export class Load implements Action {
    readonly type = VideoConsolesActionTypes.Load;
}

export class LoadSuccess implements Action {
    readonly type = VideoConsolesActionTypes.LoadSuccess;

    constructor(public payload: VideoConsoleModel[]){}
}

export class LoadFailed implements Action {
    readonly type = VideoConsolesActionTypes.LoadFailed;

    constructor(public payload: string){}
}

export type VideoConsolesActions = 
    ToggleVideoConsoleCode | SetCurrentVideoConsole | ClearCurrentVideoConsole 
    | InitializeCurrentVideoConsole | Load
    | LoadSuccess | LoadFailed;
