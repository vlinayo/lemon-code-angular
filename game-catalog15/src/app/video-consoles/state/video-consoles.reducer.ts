import { VideoConsoleModel } from '../video-console.model';
import * as fromRoot from '../../state/app.state';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { VideoConsolesActionTypes, VideoConsolesActions } from './video-consoles.actions';

export interface VideoConsoleState {
    showVideoConsoleCode: boolean;
    currentVideoConsole: VideoConsoleModel;
    videoConsoles: VideoConsoleModel[];
}

export interface State extends fromRoot.State{
    videoConsoles: VideoConsoleState;
}

//declarar el initial state despues de las interfaces (y no en el bottom);
const initialState: VideoConsoleState = {
    showVideoConsoleCode: true,
    currentVideoConsole: null,
    videoConsoles: []
}

const getVideoConsoleFeatureState = createFeatureSelector<VideoConsoleState>('videoconsoles');

export const getShowVideoConsoleCode = createSelector(
    getVideoConsoleFeatureState,
    state => state.showVideoConsoleCode
);

export const getCurrentVideoConsole = createSelector(
    getVideoConsoleFeatureState,
    state => state.currentVideoConsole
);

export const getVideoConsoles = createSelector(
    getVideoConsoleFeatureState,
    state => state.videoConsoles
);


export const reducer = ( state: VideoConsoleState = initialState, 
    action: VideoConsolesActions ): VideoConsoleState => {
    switch (action.type) {
        case VideoConsolesActionTypes.ToggleVideoConsoleCode:
            // console.log( `existing state ${JSON.stringify(state)}`);
            // console.log( `action payload ${action.payload}`);
            return {
                ...state,
                showVideoConsoleCode: action.payload, //aqui si podemos porque payload es boolean (inmutable)
                // break;
            };
        case VideoConsolesActionTypes.SetCurrentVideoConsole:
            return {
                ...state,
                currentVideoConsole: { ...action.payload }, //aqui paso una copia del objeto (no la ref)
            };
        case VideoConsolesActionTypes.ClearCurrentVideoConsole:
            return{
              ...state,
              currentVideoConsole: null,  
            };
        case VideoConsolesActionTypes.InitializeCurrentVideoConsole:
            return{
                ...state,
                currentVideoConsole: {
                    id: 0,
                    name: '',
                    code: '',
                    description: '',
                    rating: 0
                }
            };
        case VideoConsolesActionTypes.LoadSuccess:
            return {
                ...state,
                videoConsoles: action.payload
            }
        default:
            return state;
            // break;
    }
};