import { Component, OnInit, OnDestroy } from '@angular/core';

import { VideoConsoleModel } from '../video-console.model';

import { Observable } from 'rxjs';

// import { Subscription } from 'rxjs';
// import { VideoConsoleService } from '../video-console.service';

import { Store, select } from '@ngrx/store';
import { VideoConsoleState } from '../state/video-consoles.reducer';

import * as fromVideoConsoles from '../state/video-consoles.reducer';

import * as videoConsoleActions from '../state/video-consoles.actions';

@Component({
  selector: 'app-video-console-list',
  templateUrl: './video-console-list.component.html',
  styleUrls: ['./video-console-list.component.css']
})
export class VideoConsoleListComponent implements OnInit, OnDestroy {
  errorMessage: string;
  displayCode: boolean;
  // videoConsoles: VideoConsoleModel[];
  videoConsoles$: Observable<VideoConsoleModel[]>;
  selectedVideoConsole: VideoConsoleModel | null;
  // sub: Subscription;

  constructor(
    private store: Store<fromVideoConsoles.State>,
    // private videoConsoleService: VideoConsoleService
    ) { }

    ngOnInit(): void {
      // this.sub = this.videoConsoleService.selectedVideoConsoleChanges$.subscribe(
      //   selectedVideoConsole => this.selectedVideoConsole = selectedVideoConsole
      // );
  
      this.store.pipe(select(fromVideoConsoles.getCurrentVideoConsole))
        .subscribe(
          (svc) => this.selectedVideoConsole = svc
        );
        
      // this.videoConsoleService.getVideoConsoles().subscribe(
      //   (videoConsoles: VideoConsoleModel[]) => this.videoConsoles = videoConsoles,
      //   (err: any) => this.errorMessage = err.error
      // );

      this.store.dispatch(new videoConsoleActions.Load());
      this.videoConsoles$ = this.store.pipe(select(fromVideoConsoles.getVideoConsoles))
      // .subscribe(
      //   vcs => this.videoConsoles = vcs
      // );

  
      // this.store.pipe(
      //   select('videoconsoles') // [1] operador de ngrx, pasamos string indicando la parte del store que queremos usar 
      // ).subscribe( // [2]
      //   (vcs) => {
      //     // if (vcs) { // [3]
      //       this.displayCode = vcs.showVideoConsoleCode; // [4]
      //     // }
      //   }
      // );
      
      this.store.pipe(select(fromVideoConsoles.getShowVideoConsoleCode)).subscribe(
        (s) => this.displayCode = s
      );
    }

  ngOnDestroy(): void {
    // this.sub.unsubscribe();
  }

  checkChanged(value: boolean): void {
    // this.displayCode = value;
    // this.store.dispatch({
    //   type: 'TOGGLE_VIDEO_CONSOLE_CODE',
    //   payload: value,
    // })
    this.store.dispatch(new videoConsoleActions.ToggleVideoConsoleCode(value));
  }

  newVideoConsole(): void {
    // this.videoConsoleService.changeSelectedVideoConsole(this.videoConsoleService.newVideoConsole());
    this.store.dispatch(new videoConsoleActions.InitializeCurrentVideoConsole());
  }

  videoConsoleSelected(videoConsole: VideoConsoleModel) : void {
    // this.videoConsoleService.changeSelectedVideoConsole(videoConsole);
    this.store.dispatch(new videoConsoleActions.SetCurrentVideoConsole(videoConsole));
  }
}
